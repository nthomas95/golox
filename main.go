package main

import (
	"bufio"
	"fmt"
	"os"
)

func main() {
    if len(os.Args) == 1 {
        runPrompt()
    } else if len(os.Args) == 2 {
        runFile(os.Args[1])
    } else {
        fmt.Println("Usage: jlox [script]")
    }
}

func runPrompt() {
    reader := bufio.NewReader(os.Stdin)

    for {
        _, err := fmt.Print("> ")
        if err != nil {
            panic(err)
        }

        line, err := reader.ReadString('\n')
        if err != nil {
            panic(err)
        }
        if line == "\n" {
            break
        }
        run(line)
    }
}

func runFile(fileName string) {
    fmt.Printf("File name: %s\n", fileName)

    // Read the contents of the file into memory
    b, err := os.ReadFile(fileName)
    if err != nil {
        fmt.Printf("Unable to read file: %s\n", fileName)
        panic(err)
    }

    run(string(b))
}

func run(s string) {
    fmt.Printf("%s", s)
}
